<?php
/**
 * Karto
 *
 * Créateur de cartes à jouer en SVG et PDF
 *
 * Inspiré du code de phpDocumentor pour l'utilisation (minimale) de Cilex
 */

namespace Karto;

/**
 * Charge les autoloaders
 */
require_once findAutoloader();

use \Symfony\Component\Console\Input\InputInterface;

/**
 * Classe d'Application de Karto.
 */
class Application extends \Cilex\Application
{
    const VERSION = '1.0.0-dev0';

    /**
     * Initializes all components
     */
    function __construct()
    {
        parent::__construct('Karto', self::VERSION);

        $this->addAutoloader();
        $this->setTimezone();

        $this->addCommandsForProject();
    }

    /**
     * Run the application
     * 
     * @param bool $interactive Whether to run in interactive mode.
     *
     * @return void
     */
    public function run($interactive = false)
    {
        $app = $this['console'];
        if ($interactive) {
            $app = new \Symfony\Component\Console\Shell($app);
        }

        $app->run(new \Symfony\Component\Console\Input\ArgvInput());
    }

    /**
     * Adds the commands 
     *
     * @return void
     */
    protected function addCommandsForProject()
    {
        $this->command(new \Karto\Command\GenererSvgCommand());
        $this->command(new \Karto\Command\GenererPdfCommand());
    }

    
    /**
     * Déclare l'autoloader et ajoute Karto
     *
     * @return void
     */
    protected function addAutoloader()
    {
        $this['autoloader'] = include findAutoloader();
        $this['autoloader']->add('Karto', __DIR__ . '/..');
    }


    /**
     * If the timezone is not set anywhere, set it to UTC.
     *
     * This is done to prevent any warnings being outputted in relation to using
     * date/time functions. What is checked is php.ini, and if the PHP version
     * is prior to 5.4, the TZ environment variable.
     *
     * @return void
     */
    public function setTimezone()
    {
        if (false === ini_get('date.timezone')
            || (version_compare(phpversion(), '5.4.0', '<')
            && false === getenv('TZ'))
        ) {
            date_default_timezone_set('UTC');
        }
    }

}

/**
 * Tries to find the autoloader relative to this file and return its path.
 *
 * @throws \RuntimeException if the autoloader could not be found.
 *
 * @return string the path of the autoloader.
 */
function findAutoloader()
{
    $autoloader_base_path = '/../vendor/autoload.php';

    // if the file does not exist from a base path it is included as vendor
    $autoloader_location = file_exists(__DIR__ . $autoloader_base_path)
        ? __DIR__ . $autoloader_base_path
        : __DIR__ . '/../..' . $autoloader_base_path;

    if (!file_exists($autoloader_location)) {
        throw new \RuntimeException(
            'Unable to find autoloader at ' . $autoloader_location
        );
    }

    return $autoloader_location;
}
