<?php

/**
 * Commande pour générer un PDF d'une planche de cartes
 */

namespace Karto\Command;

use Cilex\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Générer des cartes.
 */
class GenererPdfCommand extends \Cilex\Command\Command
{
    protected function configure()
    {
        $this
            ->setName('generer:pdf')
            ->setDescription('Générer un fichier PDF des cartes')
            ->addArgument('conf', InputArgument::REQUIRED, 'Chemin vers la configuration Yaml des cartes')
            ->addArgument('dest', InputArgument::OPTIONAL, 'Chemin du fichier PDF créé');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $conf = $input->getArgument('conf');
        $dest = $input->getArgument('dest');

        $this->genererPDF($input, $output, $conf, $dest);
    }


    public static function genererPDF(InputInterface $input, OutputInterface $output, $conf, $dest) {
        $chemins_svg = GenererSvgCommand::genererSVG($input, $output, $conf, $dest);
        $chemins_pdf = array();

        // Générer les différents PDFs depuis les SVG
        foreach ($chemins_svg as $chemin_svg) {
            $chemin_pdf = substr($chemin_svg, 0, -4) . '.pdf';
            $chemins_pdf[] = $chemin_pdf;

            $texte = "Générer Pdf : $chemin_pdf";
            $output->write(str_pad($texte, 60, ' '));

            # http://tavmjong.free.fr/INKSCAPE/MANUAL/html/CommandLine-Export.html
            $commande = "inkscape -f '$chemin_svg' -A '$chemin_pdf'";
            $options = " --export-area-page --export-dpi 300";
            exec($commande.$options, $out, $err);

            $output->writeln("<info>[OK]</info>");
        }

        // Fusionner les différents PDFs
        if (count($chemins_pdf) > 1) {
            $un = substr($chemins_pdf[0], 0, -6);
            $all = $un."-all.pdf";
            $liste = implode("' '", $chemins_pdf);
            $texte = "Fusionner Pdfs : $all";
            $output->write(str_pad($texte, 60, ' '));
            $commande = "pdftk '$liste' cat output '$all'";
            exec($commande, $out, $err);

            $output->writeln("<info>[OK]</info>");
        }
    }
}
