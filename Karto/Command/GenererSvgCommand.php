<?php

/**
 * Commande pour générer un SVG d'une planche de cartes
 */

namespace Karto\Command;

use Cilex\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Générer des cartes.
 */
class GenererSvgCommand extends \Cilex\Command\Command
{
    protected function configure()
    {
        $this
            ->setName('generer:svg')
            ->setDescription('Générer un fichier SVG des cartes')
            ->addArgument('conf', InputArgument::REQUIRED, 'Chemin vers la configuration Yaml des cartes')
            ->addArgument('dest', InputArgument::OPTIONAL, 'Chemin du fichier SVG créé');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $conf = $input->getArgument('conf');
        $dest = $input->getArgument('dest');

        $this->genererSVG($input, $output, $conf, $dest);
    }

    // genere le fichier SVG
    public static function genererSVG(InputInterface $input, OutputInterface $output, $conf, $dest) {
        $config  = new \Karto\Config\Config($conf);
        $nom = $config->get('nom');
        $texte = "Générer Svg : $nom";

        $output->write(str_pad($texte, 60, ' '));

        $generer = new \Karto\Generer\Svg($config);
        $chemin_svg = $generer->generer($dest);

        $output->writeln("<info>[OK]</info>");

        return $chemin_svg;
    }
}
