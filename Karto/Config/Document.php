<?php

/**
 * Définit des points particuliers sur le document
**/

namespace Karto\Config;


class Document
{
    // toutes les tailles s'expriment en mm

    /**
     * Taille du document
     * @var \Karto\Config\Coordonnees */
    public $taille = null;

    /**
     * Marges du document,
     * c'est à dire le décalage entre le (0,0)
     * et l'angle de la première carte
     * @var \Karto\Config\Coordonnees */
    public $marge  = null;

    /**
     * Espacement de la grille du document
     * Cela correspond aux tailles des cartes !
     * @var \Karto\Config\Coordonnees */
    public $grille = null;

    /**
     * Nombre d'éléments dans la grille par rapport à la taille du document
     * @var \Karto\Config\Coordonnees */
    public $nombre = null;

    /**
     * Nombre total de carte sur 1 document
     * @var int
     */
    public $total = 0;

    /**
     * Coeficient multiplicateur pour passer en pixels
     * @var float
     */
    public $coef_pixel = 0;


    // Contructeur : définit le document
    public function __construct(Config $config) {
        $this->coef_pixel = 1260 / 355.60;
        $this->definir($config);
    }

    // Définit le document
    public function definir(Config $config) {

        // taille du document
        $docX = $config->get('tailleX');
        $docY = $config->get('tailleY');
        $this->taille = new Coordonnees($docX, $docY);

        // taille des cartes
        $cX   = $config->get('cartes/X'); 
        $cY   = $config->get('cartes/Y');
        $this->grille = new Coordonnees($cX, $cY);

        // marge minimales autour du document
        $marge_mini = $config->get('margeMini');

        // combien peut on loger de cartes dans la largeur et hauteur ?
        $nX = floor(($docX - 2*$marge_mini) / $cX);
        $nY = floor(($docY - 2*$marge_mini) / $cY);
        $this->nombre = new Coordonnees($nX, $nY);
        $this->total = $nX * $nY;
        
        // Calcul des marges
        // logiquement c'est la moitié de la marge restante
        $oX = ($docX - ($nX * $cX)) / 2;
        $oY = ($docY - ($nY * $cY)) / 2;
        $this->marge = new Coordonnees($oX, $oY);

    }

}
