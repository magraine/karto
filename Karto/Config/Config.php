<?php

/**
 * Lit et utilise une configuration de planche de cartes à jouer
**/

namespace Karto\Config;

use Symfony\Component\Yaml;

class Config
{
    public $conf = null;

    public $dir = "";

    // lit une config
    public function __construct($config_path)
    {

        if (!file_exists($config_path)) {
            throw new \InvalidArgumentException(
                $config_path . ' is not a valid path to the configuration'
            );
        }

        // définir le répertoire contenant la config
        $this->dir = dirname($config_path);

        $fullpath = explode('.', $config_path);

        switch (strtolower(end($fullpath))) {
            case 'yml':
            case 'yaml':
                $parser = new Yaml\Parser();
                $result = new \ArrayObject(
                    $parser->parse(file_get_contents($config_path))
                );
                break;
            case 'xml':
                $result = simplexml_load_file($config_path);
                break;
            case 'json':
                $result = json_decode(file_get_contents($config_path));
                break;
            default:
                throw new \InvalidArgumentException(
                    'Unable to load configuration; the provided file '
                    .'extension was not recognized. Only yml, json or xml allowed'
                );
        }

        $this->conf = $result;
        return $result;
    }


    // obtenir quelque chose
    // peut demander 'truc/muche' pour obtenir $conf[truc][muche]
    public function get($quoi = null, $defaut = null) {
        if (is_null($quoi)) return is_null($this->conf) ? $defaut : $this->conf;

        $quoi = explode ('/', $quoi);
        $conf = $this->conf;
        foreach ($quoi as $q) {
            if (array_key_exists($q, $conf)) {
                $conf = $conf[$q];
            } else {
                return $defaut;
            }
        }

        return $conf;
    }

    // obtenir le répertoire de travail
    public function getDir() {
        return $this->dir;
    }


    // getter !
    public function __get($name) {
        return $this->get($name);
    }
    
    // testeur !
    public function __isset($name) {
        return array_key_exists($name, $this->conf);
    }
}
