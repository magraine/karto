<?php

/**
 * Définit des points particuliers sur le document
**/

namespace Karto\Config;


class Coordonnees
{
    // toutes les tailles s'expriment en mm

    // taille X
    public $X  = 0;

    // taille Y
    public $Y  = 0;

    // Constructeur : définit X et Y
    public function __construct($X = 0, $Y = 0)
    {
        $this->set($X, $Y);
    }

    // Définit X et Y
    public function set($X = 0, $Y = 0) {
        $this->X = $X;
        $this->Y = $Y;
    }
}
