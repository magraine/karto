<?php


namespace Karto\Generer;


/**
 * Générer des cartes.
 */
class Svg
{

    // Config 
    public $conf = null;
    // Destination des svg
    public $dest = '';

    // Configurateur
    public function __construct(\Karto\Config\Config $conf) {
        $this->init($conf);
    }

    // Initialisation
    public function init(\Karto\Config\Config $conf) {
        $this->conf = $conf;
    }

    // trouver le template
    // retourne le chemin du fichier créé
    public function generer($destination = "") {
        $template = $this->conf->get('template');
        if (!$template) {
            throw new \RuntimeException('Pas de template défini !');
        }
        
        $this->dest = $destination ? $destination : '_done';
        if (!is_dir($this->dest)) @mkdir($this->dest);
        if (!is_dir($this->dest)) {
            throw new \RuntimeException("Destination $this->dest introuvable!");
        }

        $dir = $this->conf->getDir();
        $template = $dir . '/' . $template;

        $twig = new \Karto\Generer\Twig($template, $this->conf);
        $env = $this->creerEnvironnement();

        $files = array();

        // génère autant de svg que de planches de jeu de cartes à faire
        for ($i = 1; $i <= $env['planches']; $i++) {
            $env['planche'] = $i;
            // creer le svg
            $svg = $twig->composer( $env );
            // le sauver !
            $nom = $this->conf->get('nom', 'karto');
            // suffixer du numero de planche s'il y en a plus d'une
            if ($env['planches'] > 1) { $nom .= "-$i";}
            $files[] = $this->enregistrer($svg, $nom);
        }

        return $files;
    }


    // créer l'environnement de Twig
    public function creerEnvironnement() {
        // la base, c'est les définitions des cartes
        // et les infos sur le document à générer
        $env = array(
            'def' => $this->conf,
            'doc' => new \Karto\Config\Document($this->conf),
            'planches' => 1, // nombre total de planches
            'planche'  => 1,  // la planche en cours
        );

        // calcul du nombre de planches (et de fichiers svg à faire)
        $jeu = $this->conf->get('jeu');
        $env['planches'] =  (int)floor(count($jeu) / $env['doc']->total) + 1;
        return $env;
    }


    // enregistrer le svg
    public function enregistrer($svg, $nom_du_fichier) {
        $dest = $this->dest . '/' . $nom_du_fichier . '.svg';
        file_put_contents($dest, $svg);
        return $dest;
    }
    
}
