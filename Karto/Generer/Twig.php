<?php


namespace Karto\Generer;


/**
 * Générer un template Twig.
 */
class Twig
{

    // config de Karto
    public $conf = null;

    // dir & template
    public $dir = "";
    public $template = "";

    // Twig
    public $twig = null;

    // Définir
    public function __construct($template, \Karto\Config\Config $conf) {
        $this->conf = $conf;

        $this->dir = dirname($template);
        $this->template = basename($template);
        $loader = new \Twig_Loader_Filesystem($this->dir);
        // on ajoute les templates par défaut (le z-karto de base !)
        $loader->addPath('templates-dist');
        #$loader->prependPath('xxx');
       
        $twig = new \Twig_Environment($loader, array(
            'debug' => true,
        ));
        $twig->addExtension(new \Twig_Extension_Debug());
        $twig->addFilter('pixels',   new \Twig_Filter_Function('\Karto\Generer\Twig::pixels'));
        $twig->addFilter('decouper', new \Twig_Filter_Function('\Karto\Generer\Twig::decouper'));
        $twig->addFilter('imaginer', new \Twig_Filter_Function('\Karto\Generer\Twig::imaginer'));
        
        $this->twig = $twig;
    }

    // Générer le twig
    public function composer($data) {
        return $this->twig->render($this->template, $data);
    }


    // Passe une taille depuis mm vers pixels
    // selon le coef d'inkscape
    public static function pixels($mm) {
        static $coef = null;
        if (is_null($coef)) {
            $coef = 1260 / 355.60;
        }
        return round($mm * $coef, 3);
    }


    // Découpe un texte sur ses espaces et signe ·
    public static function decouper($texte) {
        $texte = explode(" ", $texte);
        $res = array();
        foreach ($texte as $t) {
            $t = explode('·', $t);
            $t[] = " ";
            $res = array_merge($res, $t);
        }
        array_pop($res);
        return $res;
    }


    // calcule depuis une image donnée
    // - le mime-type
    // - le base64encode
    // - la taille X et Y
    public static function imaginer($chemin_image, $maxX = 0, $maxY = 0) {
        $infos = array();
        if (file_exists($chemin_image)) {
            $finfo = pathinfo($chemin_image);
            $ext = $finfo['extension'];
            $im = file_get_contents($chemin_image);

            if ($ext == 'svg') {
                $xml = simplexml_load_string($im);
                $xmlattributes = $xml->attributes();
                $infos = array();
                $infos['ext'] = $ext;
                $infos['X'] = $x = $xmlattributes->width;
                $infos['Y'] = $y = $xmlattributes->height;
                $infos['mime'] = 'image/svg+xml';
                // virer le < ?xml ... ? >
                $svg = $xml->asXML();
                $svg = explode("\n", $svg);
                array_shift($svg);
                $svg = implode("\n", $svg);
                $infos['contenu'] = $svg;
            } else {
                $infos = getimagesize($chemin_image);
                $infos['ext'] = $ext;
                $infos['X'] = $x = $infos[0];
                $infos['Y'] = $y = $infos[1];
                $infos['contenu'] = "base64," . base64_encode($im);
            }


            // calcul d'un ratio comme image_passe_partout
            if ($maxX || $maxY) {
                // image origine plus large que la destination
                // il faut réduire par la largeur
                // 2 / 1  | 3 / 1
                // 1 / 1  | 3 / 1
                if (($maxX / $maxY) < ($x / $y)) {
                    $newX = $maxX;
                    $newY = $y / $x * $maxX;
                }
                // image origine plus haute que la destination
                // il faut réduire par la hauteur
                else {
                    $newX = $x / $y * $maxY;
                    $newY = $maxY;
                }
                $infos['newX'] = $newX;
                $infos['newY'] = $newY;
            }
        } else {
            throw new \RuntimeException("Image $chemin_image introuvable!");
        }
        return $infos;
    }
}
